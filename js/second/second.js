/* 
Циклы в программировании используются для многократного выполнения набора инструкций
*/

'use strict';

let userInput = null;

do {
    userInput = Number(prompt('Enter an integer, please'));
} while (!Number.isInteger(userInput));

for (let i = 0; i < userInput; i += 5) {
   console.log(i); 
};

if (userInput < 0) {
    alert('Sorry, no numbers');
} 

