'use strict';

const isPrime = integer => {
    let temp = integer + '';
    if (integer < 0) return false; 
    if (Number.isInteger(integer / 2) || Number.isInteger(integer / 3)) return false;
    if (temp[temp.length - 1] == '5') return false;
    return true;
}

let lesserNumber = null;
let biggerNumber = null;

do {
    lesserNumber = Number(prompt('Enter a lesser number, please'));
    biggerNumber = Number(prompt('Enter a bigger number, please'));
} while (!(lesserNumber < biggerNumber) && !Number.isInteger(lesserNumber) && !Number.isInteger(biggerNumber))

for (let i = lesserNumber; i < biggerNumber; i++) {
    if(isPrime(i)) console.log(i);
}
