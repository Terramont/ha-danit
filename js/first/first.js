/* var - старый тип объявления переменных, 
ограничивается областью видимости функции или
областью видидмости скрипта, видна за пределами блоков, циклов, условий.
Переменная всплывает в самое начало блока, но при этом присваивается именно
в той строке, в которой мы указали присвоение, изначально значение == undefined.
Let и const имеют блочную область видимости, не всплывают. 
Отличие только в том, что let можно переприсваивать значение а const - нельзя.*/

'use strict';

let age = null;
let name = '';

do {
    name = prompt('What\'s your age?').trim();
} while (!name);

do {
    age = Number(prompt('What\'s your name?').trim());
} while (!age || age <= 0 || !isNaN(age))

if (age < 18) {
    alert('You are not allowed to visit this website')
} else if (age < 22) {
    let res = confirm('Are you sure you want to continue?');
    if (res) alert('Welcome, ' + name);
    else alert('You are not allowed to visit this website');
} else {
    alert('Welcome, ' + name);
}
